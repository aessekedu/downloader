#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>

int debugSetting = 1;
/*
 * Look at paper references he gave us
 *
 * TODO:
 * - get LIST command to work
 * - get GET command to work, download files in chunks
 * of 1000 bytes
 * - Quit command may not work as expected?
 */

void debug(char *);
void menu();
char get_choice();
int open_connection();
void list_files(int s);
void download_file(int s);
void close_connection(int s);

int main() {
    int socket = open_connection();

    while(1) {
        menu();
        char c = get_choice();
        switch(c) {
            case 'l':
            case 'L':
                list_files(socket);
            break;
            case 'D':
            case 'd':
                download_file(socket);
            break;
            case 'Q':
            case 'q':
                close_connection(socket);
                return 0;
            break;
            default:
                menu();
            break;
        }
    }
}

void list_files(int sockfd) {
    debug("Listing files");
    printf("==========\n");
    //set up send buffer
    char sbuff[100];

    sprintf(sbuff, "LIST\n");
    size_t sendvar = send(sockfd, sbuff, strlen(sbuff), 0);
   
    //set up receive buffer
    char rbuff[1000];

    int resultcode;
    size_t count = recv(sockfd, rbuff, 1000, 0);
    int varcount = sscanf(rbuff, "+OK %d", &resultcode); 
    printf("Output: %s\n", rbuff);
    printf("==========\n\n");
}

void download_file(int sockfd) {
    printf("Pick a file to download\n");
    //list_files(sockfd);

    char *line = readline("Choice: ");
    
    //set up send and receive buffer
    char sbuff[100];
    char rbuff[100];
    int resultcode;

    //get size of file first
    sprintf(sbuff, "SIZE %s\n", line);
    send(sockfd, sbuff, strlen(sbuff), 0);
    recv(sockfd, rbuff, 1000, 0);

    int totalSize = atoi(rbuff);
    printf("Total size detected: %s\n", rbuff);

    //get file
    sprintf(sbuff, "GET %s\n", line);
    send(sockfd, sbuff, strlen(sbuff), 0);
    
    //set up receive buffer
    size_t size;
    int count = 0;
    int n;

    printf("Total size: %d\n", totalSize);
    while(totalSize != 0) {
        size = recv(sockfd, rbuff, 1000, 0);
        printf("Receiving chunk %d of size %d\n", count, size);
        count++;
        totalSize -= n;

        printf("Size of request: %d\n", size);
        printf("Total size left: %d\n", totalSize);
        fwrite(rbuff, size, 1, stdout);
    }
    
    close_connection(sockfd);
    /*
    int resultcode;
    size_t count = recv(sockfd, rbuff, 1000, 0);
    int varcount = sscanf(rbuff, "+OK %d", &resultcode);
    printf("Output: %s\n", rbuff);
    */
}

void close_connection(int sockfd) {
    close(sockfd); //supposed to segfault??
    printf("%s", "Connection closed\n");
}

/*
 * ALL WORKS BELOW
 *
 */
//just debug messages for me
void debug(char *msg) {
    if(debugSetting == 1) {
        printf("%s\n", msg);
    }
}

void menu() {
    printf("L)ist files\n");
    printf("D)ownload a file\n");
    printf("Q)uit\n\n");
}

//get first char of input
char get_choice() {
    char *line = readline("Choice: ");
    char first = line[0];
    //free(line);
    return first;
}

//connection to runwire.com, same as $ nc runwire 1234
int open_connection() {
    struct sockaddr_in sock;
    int sockfd;
    debug("Connecting to socket");

    struct hostent *h = gethostbyname("runwire.com");
    struct in_addr *ip = (struct in_addr *) h->h_addr_list[0];
    
    debug("Creating socket structure...");
    
    sock.sin_family = AF_INET;
    sock.sin_port = htons(1234);
    sock.sin_addr = *((struct in_addr *) ip);

    sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1) {
        fprintf(stderr, "Can't create socket\n");
        exit(3);
    }

    int res = connect(sockfd, (struct sockaddr *)&sock, sizeof(sock));
    if (res == -1) {
        fprintf(stderr, "Can't connect\n");
        exit(2);
    } 
    
    debug("Connected!");
    
    //swallow up +OK Greetings
    //set up receive buffer
    char rbuff[1000];

    int resultcode;
    size_t count = recv(sockfd, rbuff, 1000, 0);
    int varcount = sscanf(rbuff, "+OK %d", &resultcode); 
    printf("Output: %s\n", rbuff);

    return sockfd;
}

/*
+OK 29834 //output from server
int count = sscanf(buf, "+OK %d", &size); 
//check for +OK first, can do by assigning to int,
//check val of count to se how many vars were successfully scanned
//
//first place a null on end to make sure 
//the scanning odesn't run off the end
//
//avoid trying to hold the entire file in mem
//download 1000 bytes at a ttime
//  in a loop: recv 1000 bytes
//      fwrite
*/
