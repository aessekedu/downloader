CC=clang
CFLAGS=-g

downloader: downloader.o
	clang downloader.o -o downloader -l readline

downloader.o: downloader.c
	clang -g -c downloader.c -Wall

test: downloader
	./downloader

check: downloader
	valgrind ./downloader 
